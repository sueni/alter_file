require "../../processor_spec_helper"

describe VimPlugin::Processor do
  autoload = AlterFile::Path.new(
    "/home/user/.dotfiles/nvim/pack/local/start/alternative-file/autoload/alternative_file.vim"
  )
  plugin = AlterFile::Path.new(
    "/home/user/.dotfiles/nvim/pack/local/start/alternative-file/plugin/alternative-file.vim"
  )

  it "autoload to plugin should work" do
    VimPlugin::Processor.new(autoload).convert.should eq plugin.absolute
  end

  it "plugin to autoload should work" do
    VimPlugin::Processor.new(plugin).convert.should eq autoload.absolute
  end
end
