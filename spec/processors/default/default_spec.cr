require "../../processor_spec_helper"

describe Default::Processor do
  it "should always return empty string" do
    path = AlterFile::Path.new "spec/vpn/codes_spec.cr"
    Default::Processor.new(path).convert.should eq ""
  end
end
