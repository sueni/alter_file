require "../../processor_spec_helper"

describe Crystal::Processor do
  spec = AlterFile::Path.new(
    "/home/user/.code/projects/alter_file/spec/processor/vim_plugin/processor_spec.cr"
  )
  source = AlterFile::Path.new(
    "/home/user/.code/projects/alter_file/src/processor/vim_plugin/processor.cr"
  )

  it "source to spec should work" do
    Crystal::Processor.new(source).convert.should eq spec.absolute
  end

  it "spec to source should work" do
    Crystal::Processor.new(spec).convert.should eq source.absolute
  end
end
