require "../../processor_spec_helper"

describe Ruby::Processor do
  test = AlterFile::Path.new(
    "/home/user/.code/projects/webm/test/video_dimensions/restrained_dimension_test.rb"
  )
  source = AlterFile::Path.new(
    "/home/user/.code/projects/webm/lib/video_dimensions/restrained_dimension.rb"
  )

  it "source to test should work" do
    Ruby::Processor.new(source).convert.should eq test.absolute
  end

  it "test to source should work" do
    Ruby::Processor.new(test).convert.should eq source.absolute
  end
end
