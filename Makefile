CRYSTAL ?= crystal
SRCDIR = $(CURDIR)/src
SOURCES = $(shell find src/ -type f -name '*.cr')
NAME = $(shell basename $(CURDIR))
BINDIR = $(HOME)/.bin
TARGET = $(BINDIR)/$(NAME)
DEV_TARGET = $(MYTMPDIR)/$(NAME)

$(TARGET): $(SOURCES)
	@mkdir -p $(BINDIR)
	@$(CRYSTAL) build --no-color --release --no-debug $(SRCDIR)/$(NAME).cr -o $(TARGET)

.PHONY: check
check:
	@$(CRYSTAL) run --no-color --no-codegen $(SRCDIR)/$(NAME).cr

.PHONY: dev
dev:
	@$(CRYSTAL) build --no-color $(SRCDIR)/$(NAME).cr -o $(DEV_TARGET)

.PHONY: force
force:
	@mkdir -p $(BINDIR)
	@$(CRYSTAL) build --no-color --release --no-debug $(SRCDIR)/$(NAME).cr -o $(TARGET)
