module AlterFile
  struct Path
    def initialize(@input_file : String)
    end

    def absolute
      File.expand_path(@input_file)
    end

    def dirname
      File.dirname(File.expand_path(@input_file))
    end

    def filename
      File.basename(@input_file, File.extname(@input_file))
    end

    def extname
      File.extname(@input_file)
    end
  end
end
