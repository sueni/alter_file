class String
  def relative_path
    lchop "#{Dir.current}/"
  end
end
