require "./string_extensions"
require "./path"
require "./processors"

module AlterFile
  input_file = ARGV.first? || abort "Need input file"
  path = Path.new input_file

  processor = Processors.pick_processor path
  print processor.convert.try &.relative_path
end
