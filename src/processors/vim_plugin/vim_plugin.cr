require "../abstraction/generic_processor"
require "./autoload_path"
require "./plugin_path"

module VimPlugin
  struct Processor < GenericProcessor
    def convert
      (is_autoload? ? autoload : plugin).convert
    end

    private def autoload
      AutoloadPath.new(
        native_directory: "autoload",
        partner_directory: "plugin",
        path: path
      )
    end

    private def plugin
      PluginPath.new(
        native_directory: "plugin",
        partner_directory: "autoload",
        path: path
      )
    end

    private def is_autoload?
      path.absolute.includes? "/autoload/"
    end
  end
end
