require "../abstraction/generic_path_processor"

module VimPlugin
  struct AutoloadPath < GenericPathProcessor
    private def filename_convertor
      ->(filename : String) { filename.gsub('_', '-') }
    end
  end
end
