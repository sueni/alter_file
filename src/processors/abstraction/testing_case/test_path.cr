require "../generic_path_processor"

module TestingCase
  struct TestPath < GenericPathProcessor
    private def filename_convertor
      ->(filename : String) { filename.rchop "_#{native_directory}" }
    end
  end
end
