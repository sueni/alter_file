require "../generic_path_processor"

module TestingCase
  struct SourcePath < GenericPathProcessor
    private def filename_convertor
      ->(filename : String) { "#{filename}_#{partner_directory}" }
    end
  end
end
