require "../generic_processor"
require "./test_path"
require "./source_path"

module TestingCase
  abstract struct Processor < GenericProcessor
    private abstract def test_directory
    private abstract def source_directory

    def convert
      (is_test? ? test : source).convert
    end

    private def test
      TestPath.new(
        native_directory: test_directory,
        partner_directory: source_directory,
        path: path
      )
    end

    private def source
      SourcePath.new(
        native_directory: source_directory,
        partner_directory: test_directory,
        path: path
      )
    end

    private def is_test?
      path.filename.ends_with? "_#{test_directory}"
    end
  end
end
