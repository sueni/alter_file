abstract struct GenericProcessor
  getter path

  abstract def convert

  def initialize(@path : AlterFile::Path)
  end
end
