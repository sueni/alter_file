abstract struct GenericPathProcessor
  getter native_directory : String
  getter partner_directory : String
  getter path : AlterFile::Path

  private abstract def filename_convertor

  def initialize(@native_directory, @partner_directory, @path)
  end

  def convert
    File.join converted_directory, converted_filename
  end

  private def converted_directory
    dirname_with_trailing_slash
      .sub("/#{native_directory}/", "/#{partner_directory}/")
  end

  private def dirname_with_trailing_slash
    "#{path.dirname.rchop('/')}/"
  end

  private def converted_filename
    filename_convertor.call(path.filename) + path.extname
  end
end
