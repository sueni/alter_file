require "../abstraction/testing_case/processor"

module Crystal
  struct Processor < TestingCase::Processor
    private def test_directory
      "spec"
    end

    private def source_directory
      "src"
    end
  end
end
