require "../abstraction/testing_case/processor"

module Ruby
  struct Processor < TestingCase::Processor
    private def test_directory
      "test"
    end

    private def source_directory
      "lib"
    end
  end
end
