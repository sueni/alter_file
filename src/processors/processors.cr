require "./crystal"
require "./vim_plugin"
require "./ruby"
require "./default"

module Processors
  extend self

  def processors
    {
      ".cr"  => Crystal::Processor,
      ".rb"  => Ruby::Processor,
      ".vim" => VimPlugin::Processor,
    }
  end

  def pick_processor(path)
    processors.fetch(
      path.extname,
      Default::Processor
    ).new(path)
  end
end
